# Klausurtagung des FSR Mathematik

Wilkommen, super das du bei der KlaTaMa (Klausurtagung der Mathematik) mittmachst.
Um die Veranstaltung durchzuführen wird Gitlab verwendet um mit der Planung und Organisation zu helfen.
Wie du das ganze machst, erfährst du in den...  
[Benutzerregeln](https://gitlab.com/killer2alex/KlaTaMa/blob/master/CONTRIBUTING.md)


Sobald du dann soweit bist, auf zum...  
[Aufgabenbrett](https://gitlab.com/killer2alex/KlaTaMa/boards)

Sollte es fragen geben, dann übernimmt:
- Alex (@killer2alex) übernimmt den Ablauf der Veranstaltungen und das Konzept
- Andre (@andre.prater) übernimmt die Organisation und Freizeitgestaltung

Alle weiteren interessanten Informationen findest du hier:
- Hier gehts zum [Grobplan](#6)
- Hier geht es zur [Testaufgabe](#35)
- Hier der Link zu [Präsentationshilfen](\Praesentieren.md)

Du brauchst besondere Materialien für deinen Vortrag? Schau mal [hier](#36).

<!---
- [Teilnehmerinformation](https://gitlab.com/killer2alex/KlaTaMa/blob/master/Teilnehmer.md)
--> 
