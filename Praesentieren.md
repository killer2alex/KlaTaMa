# Allgemein zum Präsentieren

Auf der KlaTaMa wird es einige Vorträge geben. Dieses Dokument soll die Vortragenden dabei unterstützen, einen tollen Vortrag zu erarbeiten und zu präsentieren:

## Eine Bitte...

Ich möchte keinen "Schulvortag" haben! Was ein "Schulvortrag" ist? Bei einem "Schulvortrag" ist stehen die Zuschauer nicht im "Vordergrund" des Vortrags. Viel mehr ist es die Lehrperson, die begnügt wird. Da werden auch mal Sachverhalte mit hereingenommen, die man selbst nicht versteht oder auch für alle anderen nicht interessant sind. Es wird eher schnell durchgehascht, um es hinter sich zu bringen, als wirklich Inhalt zu vermitteln. Anstelle einer motivierendee Präsentationen die Interesse weckt, kommen langweilige Vorträge über abstrakte Konzepte ohne Beispiele und anstelle von begeisternden Erzählungen tritt ein Faktengebrabbel auf.

Um solche Vorträge zu vermeiden, wurden folgenden Vorschläge gesammelt: 

## Allgemeines
Hier gibt einig Hinweise zum Halten von Vorträgen:
- https://karrierebibel.de/vortrag-halten/
- https://karrierebibel.de/praesentationstechniken/
- https://karrierebibel.de/praesentationstipps/
- https://karrierebibel.de/rede-regeln/

Ein guter Vortrag startet jedoch mit einer guten Zielvorgabe. Und für diese bin ich verantwortlich. Für einige Vorträge gibt es in der Aufgabenbeschreibung eine Anzahl an Themenzielen. Neben diesen gibt es einige Hinweise, die ihr weiter unten zu eurem Vortragstyp findet (dienen als Hilfe und Richtlinie - müssen nicht eingehalten werden).  

Gerne stehe ich für alle Fragen zur Verfügung. Dafür macht ihr am besten ein Kommentar mit dem Problem in eurer Aufgabe und hollt mich dazu(@killer2alex). 

## Rollenvortrag

Rollenvorträge sollen den anderen Leuten im FSR zeigen, was alles Teil der eigenen Rolle ist und welche Aufgaben dazugehören. Die Zuschauer sollen dabei erfahren, was mögliche Probleme sind und wann diese auftreten. Im Nachhinein soll überlegt werden, was wir tun können um die Probleme zu lösen (exra Veranstaltung und nicht teil des Vortrags). 

Leitfragen und Hinweise:
- Was ist alles Teil deines Aufgabenbereichs?
- An welchen Projekten bist du daher grundsätzlich beteiligt?
- Wie sieht der typische Arbeitsablauf aus? welche Hilfsmittel verwendest du?
- Was stört dich am meisten? 
- Was wünscht du dir von anderen, die mit dir zusammen arbeiten?
- arbeite mit Beispielen und Bildern (visualisieren und eine Beziehung aufbauen)

PS: Ihr bekommt von mir grundsätzlich keine weiteren Infos.

## Themenvortrag

Themenvorträge sollen eine informative Einführung in ein Themengebiet geben. Sie dienen der Motivation des nächsten Arbeitsprozesses und der Lieferung wichtiger Informationen.

Leitfragen und Hinweise:
- gib einen kleinen Überblick über alle wesentlichen Teile des Themas 
- veranschauliche die Hintergründe und warum das Thema interessant ist
- gehe auf Einflussfaktoren ein und wie wir, direkt oder indirekt, am Thema "beteiligt" sind.
- nutze Beispiele und bekannte Situationen, um es den Zuschauern "schmackhaft" zu machen

## Methodenvortrag

Methodenvortäge liefern Handlungsanweisungen und Motivation, eine bestimmte Methode zu verwenden. Dabei soll eine Verbindung zum zu lösenden Problem aufgebaut werden. Die Präsentation sollte zur Nutzung motivieren und dafür gute Gründe liefern.

Leitfragen und Hinweise:
- warum sollte die Methode überhaupt verwendet werden?
- Welches Problem wird gelöst? 
- Welche Vor- und Nachteile hat die Methode in deinen Augen
- nutze Beispiele für Vorteile, Nachteil und Probleme
- zeige verschiedene Werkzeuge, wie die Methode umgesetzt werden kann
- Welche best practices kennst du/ sind dir aufgefallen?
- Wie kann eine progressive Erlangung der Methode aufgebaut werden?
- Handouts sind gerne gesehen